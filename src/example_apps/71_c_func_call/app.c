#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: implement a C function that could be called from the script then
	         call it from C using the awk function call entry, as if it was an
	         awk function - this should work as there is no difference between
	         functions implemented in C and awk once they are properly registered
	Run: ./app -f test.awk
*/

/* sp is the stack pointer, a_args is the number of arguments on the stack */
mawk_cell_t *blobb(mawk_state_t *context, mawk_cell_t *sp, int num_args)
{
	int n;
	char buff[64];

	/* do something - print BLOBB and all arguments */
	printf("BLOBB! ");
	for(n = 0; n < num_args; n++)
		printf("arg%d='%s' ", n, libmawk_print_cell(context, libmawk_cfunc_arg(sp, num_args, n), buff, sizeof(buff)));
	printf("\n");

	/* set a return value (find out where the return value is on the stack,
	   using libmawk_cfunc_ret()) */
	libmawk_set_cell(context, libmawk_cfunc_ret(sp, num_args), 'f', (double)1234);

	/* return the new stack pointer - should be the one that was before
	   arguments had been pushed on the stack */
	return sp - num_args;
}


int main(int argc, char **argv)
{
	mawk_state_t *m;
	mawk_cell_t ret = libmawk_empty_cell;

	/* init a context, execute BEGIN */
	m = libmawk_initialize(argc, argv);
	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* The function is registered after BEGIN is run by libmawk_initialize;
	   this means the script can not call the function from BEGIN. If calling
	   from BEGIN is required, the 3-stage initialization shall be implemented,
	   the function registered between stage1 and stage2. */

	/* register a C function (resolved runtime) */
	if (libmawk_register_function(m, "blobb", blobb) != 0) {
		fprintf(stderr, "app: ERROR: Unable to register function blobb\n");
		return 1;
	}

	if (libmawk_call_function(m, "blobb", &ret, "fs", 42.42, "hello") == MAWK_EXER_FUNCRET) {
		char buff[32];
		printf("app: return value of func blobb '%s'\n", libmawk_print_cell(m, &ret, buff, sizeof(buff)));
		libmawk_cell_destroy(m, &ret);
	}
	else
		printf("app: error: function blobb didn't return\n");


	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

