BEGIN {
	print "script: BEGIN test.awk"
	print "Hello world!" > "/dev/hash"
}

END {
	print "end." > "/dev/hash"
	getline val < "/dev/hash"
	print "script: END: " val
}
