extern const mawk_vio_imp_t mawk_vio_hash_imp;

mawk_vio_t *mawk_vio_hash_open(mawk_state_t *MAWK, const char *name, mawk_vio_open_mode_t mode);

/* retrieve current value of the hash */
unsigned long int mawk_vio_hash_val(mawk_state_t *MAWK, mawk_vio_t *vf);

/* the application wants to signal eof to the script */
int mawk_vio_hash_eof_from_app(mawk_state_t *MAWK, mawk_vio_t *vf);
