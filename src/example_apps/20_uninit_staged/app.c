#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: multi-stage uninit: collect results calculated in the END {} block
	Run: ./app -f test.awk
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;

	/* init a context, execute BEGIN */
	m = libmawk_initialize(argc, argv);
	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* run END */
	libmawk_uninitialize_stage1(m);

	/* print variable "script_state" */
	{
		const mawk_cell_t *c;
		char buff[32];
		c = libmawk_get_var(m, "script_state");
		if (c != NULL)
			printf("app: script_state = '%s'\n", libmawk_print_cell(m, c, buff, sizeof(buff)));
		else
			printf("No such variable \"script_state\"\n");
	}

	/* free the context */
	libmawk_uninitialize_stage2(m);

	return 0;
}

