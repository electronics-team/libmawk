function fnc()
{
	print "script func ENTER"
	for(i in A)
		print "script func:", i, A[i]
	print "script func LEAVE"
}

BEGIN {
	while(1) {
		n++
		print "script begin: " n
		A[n] = n/3
		if ((n % 3) == 0)
			fnc()
	}
}

