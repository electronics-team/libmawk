function fnc(   i,line)
{
	for(i in A) {
		getline line
		print "script func:", i, A[i], line
	}
}

BEGIN {
	for(n = 0; n < 4; n++) {
		getline line
		print "script begin: ", n, line
		A[n] = line
	}
}

{
	n++
	print "script main: ", n, $0
	A[n] = $0
	if (n == 6)
		fnc()
	else if (n > 8) {
		getline line
		print "script main2: ", n, line
	}
}

