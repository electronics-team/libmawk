#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: wire stdout to a pipe and process the output of the script;
	         simple setup
	Run: ./app -f test.awk
*/

void print_pipe_pending(mawk_state_t *m, mawk_vio_t *vf, char *sep)
{
	for(;;) {
		int len;
		char buf[1024];
		len = mawk_vio_fifo_read_app(m, vf, buf, sizeof(buf)-1);
		if (len <= 0)
			return;
		buf[len] = '\0';
		printf("<%s>\n%s</%s>\n", sep, buf, sep);
	}
}

int main(int argc, char **argv)
{
	mawk_state_t *m;
	mawk_vio_t *vf_stdin, *vf_stdout, *vf_log;

	/* init a context in stages */
	m = libmawk_initialize_stage1();              /* alloc context */

	/* set up pipes: stdin is a fifo, stdout is a fifo, stderr is app's stderr */
	libmawk_initialize_stdio(m, 0, 0, 1);

	m = libmawk_initialize_stage2(m, argc, argv); /* set up with CLI arguments */
	m = libmawk_initialize_stage3(m);             /* execute BEGIN */

	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* libmawk_append_input() operates on "/dev/stdin" as registered, if it is
	   a pipe, so it is compatible with manual setup */
	libmawk_append_input(m, "Hello world!\n");
	libmawk_run_main(m);


	/* print all the stdout the script produced so far */
	print_pipe_pending(m, m->fnode_stdout->vf, "stdout");

	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

