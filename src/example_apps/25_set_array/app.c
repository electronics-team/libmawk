#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: demonstrate how to change the value of an existing array
	Run: ./app -f test.awk | sort
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;
	double dbl;

	/* init a context, execute BEGIN */
	m = libmawk_initialize(argc, argv);
	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* setting an index in an array means existing data is overwritten
	   or new data is allocated/registered. There are calls with different
	   conventions doing the same, to make the app developer's life easier. */

	/* Vararg version, for static/hardwired data: */
	libmawk_set_array_at(m, "bar", "one", 'd', 42);

	/* Similar version with a void * pointer as value: */
	dbl=3.141592654;
	libmawk_set_array_atp(m, "bar", "two", 'f', &dbl);

	/* The above two were creating new indixes in the array; the below call
	   will overwrite an existing one created by the script */
	libmawk_set_array_at(m, "bar", "wow", 's', "no way!");

	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

