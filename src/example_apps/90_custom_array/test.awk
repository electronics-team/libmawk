BEGIN {
# fill in the array with "HELLO WORLD" from index 1; split() tries to fill
# the array with each word, but the virtual array set code will consider only
# the first character of each word
	split("Haha Error Light Lungs Old Water Opponent Riot Long Diode !", CARR, " ")

# use numbers to fill in some more, leaving a gap between the end of the
# last index used by split() and the first used by this call
	for(n = 14; n < 32; n++)
		CARR[n] = 65+n-14

# delete [4], which is the second "L"; deletion means setting to 0
# with this custom implementation since there is no way to really
# remove items from a static array
	delete CARR[4]
}
