function lib_hex_version()
{
	return 1
}

BEGIN {
	LIB_HEX["0"] = 0
	LIB_HEX["1"] = 1
	LIB_HEX["2"] = 2
	LIB_HEX["3"] = 3
	LIB_HEX["4"] = 4
	LIB_HEX["5"] = 5
	LIB_HEX["6"] = 6
	LIB_HEX["7"] = 7
	LIB_HEX["8"] = 8
	LIB_HEX["9"] = 9
	LIB_HEX["A"] = 10
	LIB_HEX["a"] = 10
	LIB_HEX["B"] = 11
	LIB_HEX["b"] = 11
	LIB_HEX["C"] = 12
	LIB_HEX["c"] = 12
	LIB_HEX["D"] = 13
	LIB_HEX["d"] = 13
	LIB_HEX["E"] = 14
	LIB_HEX["e"] = 14
	LIB_HEX["F"] = 15
	LIB_HEX["f"] = 15
}

# converts hex number in string to integer; returns "" on error
function lib_hex_str2int(s   ,v,n,l,c)
{
	sub("^0x", "", s)
	v = 0;
	l = length(s)
	for(n = 1; n <= length(s); n++) {
		c = substr(s, n, 1)
		if (!(c in LIB_HEX))
			return ""
		v = v * 16 + LIB_HEX[c];
	}
	return v
}

function lib_hex_int2str(i)
{
	return sprintf("0x%x", i)
}

