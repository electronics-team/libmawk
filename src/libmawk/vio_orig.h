extern const mawk_vio_imp_t mawk_vio_orig_imp;
extern const mawk_vio_init_t mawk_vio_orig_init;

/* set up /dev/stdin, /dev/stdout and /dev/stderr (depending on which ones
   are enabled) in the original way (stdin to fd0, stdout and stderr to the
   FILE * variants*/
void mawk_vio_orig_setup_stdio(mawk_state_t * MAWK, int enable_stdin, int enable_stdout, int enable_stderr);

/* manually enable/disable buffering of a vio_orig */
int mawk_vio_orig_setbuf(mawk_state_t *MAWK, mawk_vio_t *vf_, int buf_enable);
