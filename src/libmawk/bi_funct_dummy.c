/********************************************
bi_funct.c

libmawk changes (C) 2009-2013, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include "mawk.h"
#include "bi_funct.h"
#include "bi_vars.h"


mawk_cell_t *mawk_bi_length(mawk_state_t *MAWK, register mawk_cell_t *sp)
{
	abort();
}

char *mawk_str_str(register char *target, char *key, unsigned key_len)
{
	abort();
}


mawk_cell_t *mawk_bi_index(mawk_state_t *MAWK, register mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_substr(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_match(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_toupper(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_tolower(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

#ifndef MAWK_NO_FLOAT
mawk_cell_t *mawk_bi_sin(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_cos(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_atan2(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_log(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_exp(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}
#endif

mawk_cell_t *mawk_bi_int(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_sqrt(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


mawk_cell_t *mawk_bi_srand(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	/* called from init */
	return NULL;
}

mawk_cell_t *mawk_bi_rand(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_close(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


mawk_cell_t *mawk_bi_fflush(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_system(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


mawk_cell_t *mawk_bi_getline(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


mawk_cell_t *mawk_bi_sub(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_gsub(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


mawk_cell_t *mawk_bi_call(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_acall(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_valueof(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_split(mawk_state_t *MAWK, register mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_isnan(mawk_state_t *MAWK, register mawk_cell_t *sp)
{
	abort();
}

