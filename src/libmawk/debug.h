#include "mawk.h"

void mawk_location_change(mawk_state_t * MAWK, int new_token_line);
void mawk_debug_callstack_pop(mawk_state_t * MAWK);
void mawk_debug_callstack_push(mawk_state_t * MAWK, FBLOCK * f);

extern FBLOCK mawk_debug_main;
extern FBLOCK mawk_debug_begin;
