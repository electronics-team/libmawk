
/********************************************
vargs.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1992 Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

/* provides common interface to <stdarg.h> or <varargs.h> 
   only used for error messages
*/

#if     NO_STDARG_H
#include <varargs.h>

#ifdef  VA_ALIST
#define  VA_ALIST(mawk_state_t *MAWK; type arg)  (va_alist) va_dcl
#define  VA_ALIST2(mawk_state_t *MAWK; t1 a1 ; t2 a2) (va_alist) va_dcl
#endif

#define  VA_START(p,type, last)  va_start(p) ;\
                                 MAWK = va_arg(p,mawk_state_t *); \
                                 last = va_arg(p,type)


#define  VA_START2(p,t1,a1,t2,a2)  va_start(p) ;\
                                  MAWK = va_arg(p,mawk_state_t *); \
                                  a1 = va_arg(p,t1);\
                                  a2 = va_arg(p,t2)

#else	/* have stdarg.h */
#include <stdarg.h>

#ifndef  VA_ALIST
#define  VA_ALIST(type, arg)  (mawk_state_t *MAWK, type arg, ...)
#define  VA_ALIST2(t1,a1,t2,a2)  (mawk_state_t *MAWK, t1 a1,t2 a2,...)
#endif

#define  VA_START(p,type,last)   va_start(p,last)

#define  VA_START2(p,t1,a1,t2,a2)  va_start(p,a2)

#endif
