/* number type */
typedef double mawk_num_t;

/* constant format differ for floating point and int */
#define MAWK_NUM_ZERO 0.0
#define MAWK_NUM_ONE  1.0

/* default printf format */
#define NUM_FMT "%g"

/* format for the disassembler */
#define NUM_FMT_DA "%.6g"

#define mawk_num_sqrt sqrt
#define mawk_num_int(d) ((d) >= MAWK_NUM_ZERO ? floor(d) : ceil(d))

#ifdef MAWK_HAVE_SAFE_NAN
#define P_isnan(x)  isnan(x)
#define P_nan()     nan("nan")
#define strtonum(nptr, endptr) strtod(nptr, endptr)
#else
#define NUM_NAN HUGE_VAL
#define P_isnan(x)  ((x) == NUM_NAN)
#define P_nan()     (NUM_NAN)
#define strtonum(nptr, endptr) mawk_strtonum_(nptr, endptr)
double mawk_strtonum_(const char *nptr, char **endptr);
#endif

double mawk_num_pow(double x, double y);
double P_fmod(double x, double y);



