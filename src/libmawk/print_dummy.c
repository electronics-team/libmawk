/********************************************
print_dummy.c

libmawk changes (C) 2013, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"

mawk_cell_t *mawk_bi_print(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_printf(mawk_state_t *MAWK, register mawk_cell_t *sp)
{
	abort();
}

mawk_cell_t *mawk_bi_sprintf(mawk_state_t *MAWK, mawk_cell_t *sp)
{
	abort();
}


