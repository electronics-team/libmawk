
/********************************************
files.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef   FILES_H
#define   FILES_H

/* IO redirection types */
#define  F_IN           (-5)
#define  PIPE_IN        (-4)
#define  PIPE_OUT       (-3)
#define  F_APPEND       (-2)
#define  F_TRUNC        (-1)
#define  IS_OUTPUT(type)  ((type)>=PIPE_OUT)

/* look up a file:type by name; if it does not exist, create it (if create is not 0).
   NOTE: F_APPEND and F_TRUNC are the same in file:type. */
FILE_NODE *mawk_file_find(mawk_state_t *MAWK, mawk_string_t *name, int type, int create);
FILE_NODE *mawk_file_find_(mawk_state_t *MAWK, const char *name, int type, int create);


int mawk_file_close(mawk_state_t *, mawk_string_t *);
int mawk_file_close_(mawk_state_t *MAWK, FILE_NODE *f);
int mawk_file_flush(mawk_state_t *, mawk_string_t *);
void mawk_flush_all_output(mawk_state_t * MAWK);

/* register a mawk_vio_t * as an open file - no checks are performed about
   the name, which should be unique; the caller should use mawk_file_find
   to make sure the file does not exist
   nofin never sets up the file node's fin buffer, while the plain version
   does this for input files/pipes.
   */
FILE_NODE *mawk_file_register_nofin(mawk_state_t *MAWK, const char *name, int type, mawk_vio_t *vf);
FILE_NODE *mawk_file_register(mawk_state_t *MAWK, const char *name, int type, mawk_vio_t *vf);


#ifndef MAWK_NO_FORK
int mawk_wait_for(mawk_state_t *, int);
#endif


void mawk_file_uninit(mawk_state_t * MAWK);

#endif
