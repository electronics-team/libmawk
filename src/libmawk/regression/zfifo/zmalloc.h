#ifndef ZMALLOC_H
#define ZMALLOC_H

#include <stdlib.h>
#define mawk_zfree(MAWK, ptr, size)   free(ptr)
#define mawk_zmalloc(MAWK, size)      malloc(size)

#endif
