# test an UB: posix doesn't tell the order of eval
# intuitively right side should happen first so that the left
# side would create A[1] again.
#
# Original mawk did it the other way around, leaving A[1] empty (result: 1)
# gawk 4.0.1 does right side first and returns 7
# lmawk with the virtualized array support does the same, returns 7

function side_effect()
{
	delete A[1]
	return 6
}

BEGIN {
# UB:
	A[1] = side_effect()
	print A[1]+1
}
