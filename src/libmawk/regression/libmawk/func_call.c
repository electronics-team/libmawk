#include <stdio.h>
#include <libmawk.h>

int main(int argc, char **argv)
{
	mawk_state_t *m;
	mawk_cell_t ret = libmawk_empty_cell;

	/* init a context, execute BEGIN */
	m = libmawk_initialize(argc, argv);
	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	if (libmawk_call_function(m, "foo", &ret, "dfsQ", (int)42, (double)1.234, (char *)"test string1.", 1) == MAWK_EXER_FUNCRET) {
		char buff[32];
		printf("app: error: function retuned; return value of foo is '%s'\n", libmawk_print_cell(m, &ret, buff, sizeof(buff)));
		libmawk_cell_destroy(m, &ret);
	}
	else {
		printf("app: OK: function foo didn't return\n");
	}

	libmawk_uninitialize(m);
	return 0;
}

