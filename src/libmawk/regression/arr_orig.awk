# test whether the new array code does all these properly for an orig array
# (array with no side effect)
# This test should have the same result as with mawk or gawk
BEGIN {
	A[1] = 5
	A[1] += 2
	A[1] -= 1
	A[1]++
	A[1] *= 6
	A[1]--
	A[1] /= 6
	++A[1]
	A[1] ^= 2
	--A[1]
	A[1] %= 765
	print A[1]
}
