BEGIN {
	nan="nan"+0
	print "atan2", atan2(nan, nan), atan2(1, nan), atan2(nan, 1), atan2(1, 0)
	print "log  ", log(nan), log(0), log(-1)
	print "sqrt ", sqrt(nan), sqrt(0), sqrt(-1)
	print "exp  ", exp(nan)
	print "sin  ", sin(nan)
	print "cos  ", sin(nan)
	print "tan  ", sin(nan)
	print "int  ", int(nan)
	print "pow  ", (nan ^ nan), (1 ^ nan), (nan ^ 1), ((-1) ^ 1.2)
}
