#!/bin/sh

# bin dump round trip test: on all known scripts
#  - dump asm
#  - compile to binary, load the binary and dump asm
#  - compare the two asms
# Asms shall match for any script (except for pointer values).

# replace pointers with <<ptr>>
ptr_filt()
{
	sed "s/<<[x0-9a-fA-F]*>>/<<ptr>>/g"
}

# the test procedure for awk source $1
bintest()
{
	($LMAWK -Wdump -f $1 || return 1) | ptr_filt > $1.orig
	$LMAWK -Wcompile -f $1  > $1.bin || return 1
	($LMAWK -Wdump -b $1.bin || return 1) | ptr_filt > $1.new
	diff $1.orig $1.new || return 1
	rm $1.orig $1.new $1.bin
}

bintest $1


