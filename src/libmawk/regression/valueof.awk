function test(var, idx)
{
	if (idx == "")
		print var "=" valueof(var)
	else
		print var "=" valueof(var, idx)
}

BEGIN {
	string="hello world"
	number=42.321
	array[1] = "one"
	array[2] = "two"
	array["three"] = 3
	test("string")
	test("number")
	test("array", 1)
	test("array", 2)
	test("array", "three")
}