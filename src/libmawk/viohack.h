/* functions which are somewhat hackish and won't fit in the vio paradigm but
   are required in the lmawk executable */

/* check whether we need to be interactive and chaneg the interactive flag
   accordingly (isatty() and other heuristics) */
void mawk_detect_interactive(mawk_state_t *MAWK);
