/********************************************
dump_code.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991-93, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include "mawk.h"
#include "code.h"
#include "init.h"
#include "jmp.h"
#include "field.h"
#include "da_bin_helper.h"

void mawk_dump_code(mawk_state_t * MAWK)
{
	switch(MAWK->dump_code_flag) {
		case 1: mawk_dump_code_text(MAWK); break;
		case 2: mawk_print_code_bin(MAWK, ""); break;
		default:
			mawk_errmsg(MAWK, -1, "libmawk internal error: unknown MAWK->dump_code_flag value %d\n", MAWK->dump_code_flag);
			abort();
	}
}
