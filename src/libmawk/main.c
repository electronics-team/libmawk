
/********************************************
main.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include "mawk.h"
#include "init.h"
#include "code.h"
#include "files.h"
#include "debug.h"
#include "viohack.h"
#include "vio_orig.h"

int main(int argc, char **argv)
{
	mawk_state_t *m;
	int err = 0;
	m = mawk_initialize(argc, argv, mawk_vio_orig_init);
	if (m != NULL) {
		mawk_vio_orig_setup_stdio(m, 1, 1, 1);
		mawk_detect_interactive(m);
#ifndef MAWK_NO_COMP
		mawk_parse(m);
		if (m->compile_error_count != 0)
			err = 1;
		m->mawk_state = EXECUTION;
#ifndef MAWK_NO_EXEC
		if (m->debug_symbols)
			mawk_debug_callstack_push(m, &mawk_debug_begin);
#endif
		if ((m->compile_error_count == 0) && (!m->do_exit))
#endif
#ifndef MAWK_NO_EXEC
			mawk_execute(m, m->execution_start, m->eval_stack - 1, 0);
#endif

#ifndef MAWK_NO_EXEC
		if (m->debug_symbols)
			mawk_debug_callstack_pop(m);
#endif

		err = m->final_exit_code;
		mawk_uninitialize(m);
	}
	else
		err = 1;

	return err;
}

void mawk_exit_(mawk_state_t *MAWK, int x)
{
	MAWK->do_exit = 1;
	MAWK->final_exit_code = MAWK->rt_exit_code != 0 ? MAWK->rt_exit_code : x;
}
