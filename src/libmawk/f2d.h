/********************************************
f2d.h

libmawk changes (C) 2018, Tibor 'Igor2' Palinkas;

This is a source file for libmawk, an implementation of
the AWK programming language.

libmawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef MAWK_F2D_H
#define MAWK_F2D_H

typedef void (mawk_generic_func)();

/* C89 doesn't allow function pointer vs. data pointer casts */
union mawk_f2d_u {
	void *data;
	mawk_generic_func *func;
};

static void *mawk_f2d_(mawk_generic_func *func)
{
	union mawk_f2d_u tmp;
	tmp.func = func;
	return tmp.data;
}

static mawk_generic_func *mawk_d2f_(void *data)
{
	union mawk_f2d_u tmp;
	tmp.data = data;
	return tmp.func;
}

#define mawk_f2d(f) mawk_f2d_((mawk_generic_func *)(f))
#define mawk_d2f(d) mawk_d2f_((void *)(d))

#endif
