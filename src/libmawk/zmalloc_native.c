
/********************************************
zmalloc.c

libmawk changes (C) 2009-2012, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include  "mawk.h"
#include  "zmalloc.h"
#include  "memory.h"
#include  "nstd.h"

/* zmalloc alternative using native malloc/realloc/free */

static void out_of_mem(mawk_state_t * MAWK)
{
	static const char out[] = "out of memory";

	if (MAWK->mawk_state == EXECUTION)
		mawk_rt_error(MAWK, out);
	else {
		/* I don't think this will ever happen */
		mawk_compile_error(MAWK, out);
		mawk_exit(MAWK, 2);
	}
}

/* mawk_zmalloc() is a macro in front of mawk_bmalloc "BLOCK malloc" */
PTR mawk_bmalloc(mawk_state_t *MAWK, register unsigned blocks)
{
	PTR q;
	q = malloc(blocks << ZSHIFT);
	if (q == NULL)
		out_of_mem(MAWK);
}

void mawk_bfree(mawk_state_t *MAWK, register PTR p, register unsigned blocks)
{
	free(p);
}

PTR mawk_zrealloc(mawk_state_t *MAWK, register PTR p, unsigned old_size, unsigned new_size)
{
	PTR q;
	q = realloc(p, new_size);
	if (q == NULL)
		out_of_mem(MAWK);
}

char *mawk_zstrclone(mawk_state_t *MAWK, const char *s)
{
	int l;
	char *ret;

	if (s == NULL)
		return NULL;

	l = strlen(s);
	ret = mawk_zmalloc(MAWK, l+1);
	memcpy(ret, s, l+1);
	return ret;
}


#ifndef	 __GNUC__
/* pacifier for Bison , this is really dead code */
PTR alloca(unsigned sz)
{
	/* hell just froze over */
	exit(100);
	return (PTR) 0;
}
#endif
