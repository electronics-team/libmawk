
/********************************************
code.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  CODE_H
#define  CODE_H

#include <libmawk/memory.h>

#define  PAGESZ	512
	/* number of code instructions allocated at one time */
#define  CODEWARN        16

/* coding scope */
#define   SCOPE_MAIN    0
#define   SCOPE_BEGIN   1
#define   SCOPE_END     2
#define   SCOPE_FUNCT   3

#define mawk_code_ptr  MAWK->active_code.ptr
#define mawk_code_base MAWK->active_code.base
#define mawk_code_warn MAWK->active_code.warn
#define mawk_code_limit MAWK->active_code.limit
#define mawk_code_offset (mawk_code_ptr-mawk_code_base)

#define INST_BYTES(x) (sizeof(INST)*(unsigned)(x))

#define addloc \
		if ((MAWK->debug_symbols) && (MAWK->token_lineno != MAWK->last_token_lineno)) { \
			mawk_code_ptr++ -> op = LOCATION; \
			mawk_code_ptr++ -> op = MAWK->token_lineno; \
			MAWK->last_token_lineno = MAWK->token_lineno; \
		} \

#define  code1(x)  \
	do { \
		mawk_code_ptr++ -> op = (x); \
	} while(0)

/* shutup picky compilers */
#define  code2(MAWK, x,p)  \
	do { \
		addloc \
		mawk_xcode2(MAWK, x,(PTR)(p)); \
	} while(0)

void mawk_xcode2(mawk_state_t *, int, PTR);
void mawk_code2op(mawk_state_t *, int, int);
INST *mawk_code_shrink(mawk_state_t *, CODEBLOCK *, unsigned *);
void mawk_code_reset_size(mawk_state_t *MAWK, int size);
void mawk_code_grow(mawk_state_t * MAWK);
void mawk_set_code(mawk_state_t *);
void mawk_be_setup(mawk_state_t *, int);
void mawk_dump_code(mawk_state_t *);       /* dump code in a format dictated by MAKW->dump_code_flag */
void mawk_dump_code_text(mawk_state_t *);
void mawk_dump_sym_text(mawk_state_t *);   /* dump symbol table in text (in binary it's automatically included) */

const char *mawk_find_bi_name(PF_CP);
PF_CP mawk_find_bi_ptr(const char *name);

/*  the machine opcodes  */
/* to avoid confusion with a ptr FE_PUSHA must have op code 0 */
/* unfortunately enums are less portable than defines */

#define FE_PUSHA       0
#define FE_PUSHI       1
#define F_PUSHA        2
#define F_PUSHI        3
#define NF_PUSHI       4
#define _HALT          5
#define _RANGE_STOP    6
#define _PUSHC         7
#define _PUSHD         8
#define _PUSHS         9
#define _PUSHINT       10
#define _PUSHA         11
#define _PUSHI         12
#define L_PUSHA        13
#define L_PUSHI        14
#define AE_PUSHA       15
#define AE_PUSHI       16
#define A_PUSHA        17
#define LAE_PUSHA      18
#define LAE_PUSHI      19
#define LA_PUSHA       20
#define _POP           21
#define _ADD           22
#define _SUB           23
#define _MUL           24
#define _DIV           25
#define _MOD           26
#define _POW           27
#define _NOT           28
#define _TEST          29
#define A_TEST         30
#define A_DEL          31
#define ALOOP          32
#define A_CAT          33
#define _UMINUS        34
#define _UPLUS         35
#define _ASSIGN        36
#define _ADD_ASG       37
#define _SUB_ASG       38
#define _MUL_ASG       39
#define _DIV_ASG       40
#define _MOD_ASG       41
#define _POW_ASG       42
#define F_ASSIGN       43
#define F_ADD_ASG      44
#define F_SUB_ASG      45
#define F_MUL_ASG      46
#define F_DIV_ASG      47
#define F_MOD_ASG      48
#define F_POW_ASG      49
#define _CAT           50
#define _BUILTIN       51
#define _PRINT         52
#define _POST_INC      53
#define _POST_DEC      54
#define _PRE_INC       55
#define _PRE_DEC       56
#define F_POST_INC     57
#define F_POST_DEC     58
#define F_PRE_INC      59
#define F_PRE_DEC      60
#define _JMP           61
#define _JNZ           62
#define _JZ            63
#define _LJZ           64
#define _LJNZ          65
#define _EQ            66
#define _NEQ           67
#define _LT            68
#define _LTE           69
#define _GT            70
#define _GTE           71
#define _MATCH0        72
#define _MATCH1        73
#define _MATCH2        74
#define _EXIT          75
#define _EXIT0         76
#define _NEXT          77
#define _RANGE_CHK     78
#define _CALL          79
#define _RET           80
#define _RET0          81
#define SET_ALOOP      82
#define POP_AL         83
#define OL_GL          84
#define OL_GL_NR       85
#define _OMAIN         86
#define _JMAIN         87
#define DEL_A          88
#define LOCATION       89
/* for catching array write: */
#define _ASSIGN_ARR    90
#define _ADD_ASG_ARR   91
#define _SUB_ASG_ARR   92
#define _MUL_ASG_ARR   93
#define _DIV_ASG_ARR   94
#define _MOD_ASG_ARR   95
#define _POW_ASG_ARR   96
#define _POST_INC_ARR  97
#define _POST_DEC_ARR  98
#define _PRE_INC_ARR   99
#define _PRE_DEC_ARR   100
#define AE_PUSHA_WRARR  101
#define LAE_PUSHA_WRARR 102

/* these opcodes are used for binary dumps to hold references to different
   linked pointers - assume op to be at least 32 bits unsigned (but pointers
   are not narrower than that on any target platform). Each opcode is
   a bitmask applied onto an index integer */
#define DUMP_REPL      1 << 28
#define DUMP_NUM       2 << 28
#define DUMP_STR       3 << 28
#define DUMP_VAR       4 << 28
#define DUMP_FIELDIDX  5 << 28
#define DUMP_CALL      6 << 28
#define DUMP_SPECFIELD 7 << 28
#define DUMP_RE        8 << 28
#define DUMP_SPLIT_SPACE  9 << 28
#define DUMP_SPLIT_NULL   10 << 28

#endif /* CODE_H */
