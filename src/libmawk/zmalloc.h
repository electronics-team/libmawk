
/********************************************
zmalloc.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  ZMALLOC_H
#define  ZMALLOC_H

#include <libmawk/nstd.h>

PTR mawk_bmalloc(mawk_state_t *, unsigned);
void mawk_bfree(mawk_state_t *, PTR, unsigned);
PTR mawk_zrealloc(mawk_state_t *MAWK, register PTR p, unsigned old_size, unsigned new_size);
char *mawk_zstrclone(mawk_state_t *, const char *s);

#define mawk_zmalloc(MAWK, size)  mawk_bmalloc(MAWK, (((unsigned)size)+ZBLOCKSZ-1)>>ZSHIFT)
#define mawk_zfree(MAWK, p,size)  mawk_bfree(MAWK, p,(((unsigned)size)+ZBLOCKSZ-1)>>ZSHIFT)

#define MAWK_ZMALLOC(MAWK, type)  ((type*)mawk_zmalloc(MAWK, sizeof(type)))
#define MAWK_ZFREE(MAWK, p)       mawk_zfree(MAWK, p, sizeof(*(p)))


#endif /* ZMALLOC_H */
