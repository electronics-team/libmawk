fn=fsspcoln

echo "" >$fn.out
for fs in "[ :]" "[ :]+" "[ :]*" "A"
do
	echo "*** FS=$fs" >>$fn.out
	cat $GAWK_DIR/$fn.in | $AWK -f $GAWK_DIR/$fn.awk -F "$fs" >>$fn.out 2>&1
done
dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"

