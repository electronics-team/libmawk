fn=devfd1

cp $GAWK_DIR/devfd.in1  $fn.in
$AWK -f $GAWK_DIR/$fn.awk $fn.in $fn.in 4<$GAWK_DIR/devfd.in4 5<$GAWK_DIR/devfd.in5  >$fn.out 2>&1


dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"
rm $fn.in
