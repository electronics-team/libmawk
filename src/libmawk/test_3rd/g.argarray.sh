fn=argarray

cp $GAWK_DIR/$fn.in $fn.in
echo "" | $AWK -f $GAWK_DIR/$fn.awk $fn.in >$fn.out 2>&1
dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"
rm $fn.in
