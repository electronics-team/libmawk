fn=manyfiles

mkdir junk

echo "*** stdout:" >$fn.out
echo "foo bar
baz BAZ" | $AWK -f $GAWK_DIR/$fn.awk  >>$fn.out 2>&1

for n in foo baz
do
	echo "*** junk/$n:"
	cat junk/$n
	rm junk/$n
done >>$fn.out

dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`

announce "$?" "$fn" "$dif" "$fn.out"
rmdir junk

