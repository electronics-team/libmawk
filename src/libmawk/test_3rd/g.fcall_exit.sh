fn=fcall_exit

echo "" >$fn.out
for i in 1 2 3
do
	echo "" | $AWK -f $GAWK_DIR/$fn.awk $i >>$fn.out 2>&1
	echo "EXITVAL=$?" >> $fn.out
done
dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"

