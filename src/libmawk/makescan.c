
/********************************************
makescan.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

/* source for makescan.exe which builds the scancode[]
   via:	  makescan.exe > scancode.c
*/

#define	 MAKESCAN

#include  <stdio.h>
#include  "scan.h"
#include  "mawk.h"

void mawk_scan_init(mawk_state_t * MAWK)
{
	register char *p;

	memset(MAWK->scan_code, SC_UNEXPECTED, sizeof(MAWK->scan_code));
	for (p = MAWK->scan_code + '0'; p <= MAWK->scan_code + '9'; p++)
		*p = SC_DIGIT;
	MAWK->scan_code[0] = 0;
	MAWK->scan_code[' '] = MAWK->scan_code['\t'] = MAWK->scan_code['\f'] = SC_SPACE;
	MAWK->scan_code['\r'] = MAWK->scan_code['\013'] = SC_SPACE;

	MAWK->scan_code[';'] = SC_SEMI_COLON;
	MAWK->scan_code['\n'] = SC_NL;
	MAWK->scan_code['{'] = SC_LBRACE;
	MAWK->scan_code['}'] = SC_RBRACE;
	MAWK->scan_code['+'] = SC_PLUS;
	MAWK->scan_code['-'] = SC_MINUS;
	MAWK->scan_code['*'] = SC_MUL;
	MAWK->scan_code['/'] = SC_DIV;
	MAWK->scan_code['%'] = SC_MOD;
	MAWK->scan_code['^'] = SC_POW;
	MAWK->scan_code['('] = SC_LPAREN;
	MAWK->scan_code[')'] = SC_RPAREN;
	MAWK->scan_code['_'] = SC_IDCHAR;
	MAWK->scan_code['='] = SC_EQUAL;
	MAWK->scan_code['#'] = SC_COMMENT;
	MAWK->scan_code['\"'] = SC_DQUOTE;
	MAWK->scan_code[','] = SC_COMMA;
	MAWK->scan_code['!'] = SC_NOT;
	MAWK->scan_code['<'] = SC_LT;
	MAWK->scan_code['>'] = SC_GT;
	MAWK->scan_code['|'] = SC_OR;
	MAWK->scan_code['&'] = SC_AND;
	MAWK->scan_code['?'] = SC_QMARK;
	MAWK->scan_code[':'] = SC_COLON;
	MAWK->scan_code['['] = SC_LBOX;
	MAWK->scan_code[']'] = SC_RBOX;
	MAWK->scan_code['\\'] = SC_ESCAPE;
	MAWK->scan_code['.'] = SC_DOT;
	MAWK->scan_code['~'] = SC_MATCH;
	MAWK->scan_code['$'] = SC_DOLLAR;

	for (p = MAWK->scan_code + 'A'; p <= MAWK->scan_code + 'Z'; p++)
		*p = *(p + 'a' - 'A') = SC_IDCHAR;

}

void scan_print(mawk_state_t * MAWK)
{
	register char *p = MAWK->scan_code;
	register int c;								/* column */
	register int r;								/* row */

	printf("\n\n/* scancode.c */\n\n\n");
	printf("const char mawk_scan_code[256] = {\n");

	for (r = 1; r <= 16; r++) {
		for (c = 1; c <= 16; c++) {
			printf("%2d", *p++);
			if (r != 16 || c != 16)
				putchar(',');
		}
		putchar('\n');
	}

	printf("} ;\n");
}


int main(int argc, char **argv)
{
	mawk_state_t m, *MAWK = &m;
	mawk_scan_init(MAWK);
	scan_print(MAWK);
	return 0;
}
