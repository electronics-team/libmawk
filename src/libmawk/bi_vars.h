
/********************************************
bi_vars.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  BI_VARS_H
#define  BI_VARS_H  1


/* builtin variables NF, RS, FS, OFMT are stored
   internally in field[], so side effects of assignment can
   be handled 
*/

/* NR and FNR must be next to each other */
#define  NR        (MAWK->bi_vars+0)
#define  FNR       (MAWK->bi_vars+1)
#define  ARGC      (MAWK->bi_vars+2)
#define  FILENAME  (MAWK->bi_vars+3)
#define  OFS       (MAWK->bi_vars+4)
#define  ORS       (MAWK->bi_vars+5)
#define  RLENGTH   (MAWK->bi_vars+6)
#define  RSTART    (MAWK->bi_vars+7)
#define  SUBSEP    (MAWK->bi_vars+8)
#define  ERRNO     (MAWK->bi_vars+9)
#define  LIBPATH   (MAWK->bi_vars+10)

#define  NUM_BI_VAR  11

#ifdef MAWK_MEM_PEDANTIC
void mawk_bi_vars_uninit(mawk_state_t * m);
#endif

#endif
