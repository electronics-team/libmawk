/********************************************
libmawk changes (C) 2009-2014, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991-93, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"
#include "repl.h"
#include "cell.h"

char *mawk_bozo_cellcpy = "bad cell passed to mawk_cellcpy()";

/* does not assume target was a cell, if so
   then caller should have made a previous
   call to cell_destroy	 */

#ifdef CELLDEBUG
void DB_mawk_cellcpy(mawk_state_t *MAWK, register mawk_cell_t *target, const register mawk_cell_t *source)
{
	static int cnt = 0;
	cell_paranoia_chk(MAWK, source);
	switch (target->type = source->type) {
	case C_NOINIT:
	case C_SPACE:
	case C_SNULL:
		break;

	case C_NUM:
		target->d.dval = source->d.dval;
		break;

	case C_STRNUM:
		target->d.dval = source->d.dval;
		/* fall thru */

	case C_REPL:
	case C_MBSTRN:
	case C_STRING:
		string(source)->ref_cnt++;
		/* fall thru */

	case C_RE:
		target->ptr = source->ptr;
		break;

	case C_REPLV:
		mawk_replv_cpy(MAWK, target, source);
		break;

	default:
		mawk_bozo(MAWK, "bad cell passed to mawk_cellcpy()");
		break;
	}
}
#endif

#ifdef CELLDEBUG
#include "zmalloc.h"
void DB_cell_destroy(mawk_state_t *MAWK, register mawk_cell_t *cp)	/* HANGOVER time */
{
	switch (cp->type) {
	case C_NOINIT:
	case C_NUM:
	case C_ARR_REF:
		break;

	case C_ARR_REF_BT:
		mawk_bozo(MAWK, "ARR_REF_BT destroy");
		break;

	case C_MBSTRN:
	case C_STRING:
	case C_STRNUM:
		if (--string(cp)->ref_cnt == 0)
			mawk_zfree(MAWK, string(cp), string(cp)->len + STRING_OH);
		break;

	case C_RE:
		mawk_bozo(MAWK, "cell destroy called on RE cell");
	default:
		mawk_bozo(MAWK, "cell destroy called on bad cell type");
	}
	cell_destroy_paranoia_set(cp);
}

#endif
