#ifndef NUM_H
#define NUM_H

#include "conf.h"
#include "sizes.h"

typedef enum { /* fixed numbers are important because of the file format */
	MAWK_NUM_ID_DOUBLE = 'd',
	MAWK_NUM_ID_INT    = 'i'
} mawk_num_id_t;

extern const mawk_num_id_t mawk_num_id;

#ifdef MAWK_NO_FLOAT
#	include <libmawk/num_int.h>
#else
#	include <math.h>
#	include <libmawk/num_double.h>
#endif

Int mawk_d_to_I(mawk_num_t);
const char *mawk_num_print_spec(mawk_num_t d);


#endif /* NUM_H */
