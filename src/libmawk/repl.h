
/********************************************
repl.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  REPL_H
#define  REPL_H

PTR mawk_re_compile(mawk_state_t *, mawk_string_t *);
char *mawk_re_uncompile(mawk_state_t *, PTR);


mawk_cell_t *mawk_repl_compile(mawk_state_t *, mawk_string_t *);
char *mawk_repl_uncompile(mawk_state_t *, mawk_cell_t *);
int pmawk_repl_uncompile_bin(mawk_state_t *MAWK, mawk_cell_t *cp);
void mawk_repl_destroy(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_replv_cpy(mawk_state_t *, mawk_cell_t *, const mawk_cell_t *);
mawk_cell_t *mawk_replv_to_repl(mawk_state_t *, mawk_cell_t *, mawk_string_t *);

#endif
