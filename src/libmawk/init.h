
/********************************************
init.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  INIT_H
#define  INIT_H

#include <libmawk/symtype.h>

/* nodes to link file names for multiple
   -f option */

extern char *sprintf_buff, *sprintf_limit;


/* high levelinit: all 3 steps in order: */
mawk_state_t *mawk_initialize(int argc, char **argv, mawk_vio_init_t vio_init);

/* low level init, step 1: allocate the context and init constants*/
mawk_state_t *mawk_initialize_alloc(void);

/* set up vio and builtins here */

/* low level init, step 2: set up args and load scripts */
mawk_state_t *mawk_initialize_argv(mawk_state_t *MAWK, int argc, char **argv);

/* low level init, step 3: initialize the code */
void code_init(mawk_state_t *);



void mawk_uninitialize(mawk_state_t * m);
void code_cleanup(void);
void compile_cleanup(void);
int mawk_scan_init(mawk_state_t *, char *);
void bi_vars_init(mawk_state_t * MAWK);
void bi_funct_init(mawk_state_t *);
void print_init(void);
void mawk_kw_init(mawk_state_t * MAWK);
#ifdef MAWK_MEM_PEDANTIC
void mawk_kw_uninit(mawk_state_t * MAWK);
#endif
void mawk_field_init(mawk_state_t *);
void mawk_fpe_init(void);
void mawk_set_stderr(mawk_state_t * MAWK);
void mawk_append_input_file(mawk_state_t * MAWK, const char *fn, int bytecode);

int mawk_is_cmdline_assign(mawk_state_t *, char *);


#endif /* INIT_H  */
