
/********************************************
field.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include <stdio.h>
#include "mawk.h"
#include "field.h"
#include "init.h"
#include "memory.h"
#include "scan.h"
#include "bi_vars.h"
#include "repl.h"
#include "regexp.h"
#include "cell.h"

static void build_field0(mawk_state_t *);
static void set_rs_shadow(mawk_state_t *);
static void load_field_ov(mawk_state_t *);

static void set_rs_shadow(mawk_state_t * MAWK)
{
	mawk_cell_t c;
	mawk_string_t *sval;
	char *s;
	unsigned len;

	if (MAWK->posix_space_flag && MAWK->mawk_state == EXECUTION)
		MAWK->scan_code['\n'] = SC_UNEXPECTED;

	if (MAWK->rs_shadow.type == SEP_STR) {
		free_STRING((mawk_string_t *) MAWK->rs_shadow.ptr);
	}

	mawk_cellcpy(MAWK, &c, MAWK_RS);
	mawk_cast_for_split(MAWK, &c);
	switch (c.type) {
	case C_RE:
		if ((s = mawk_is_string_split(c.ptr, &len))) {
			if (len == 1) {
				MAWK->rs_shadow.type = SEP_CHAR;
				MAWK->rs_shadow.c = s[0];
			}
			else {
				MAWK->rs_shadow.type = SEP_STR;
				MAWK->rs_shadow.ptr = (PTR) mawk_new_STRING(MAWK, s);
			}
		}
		else {
			MAWK->rs_shadow.type = SEP_RE;
			MAWK->rs_shadow.ptr = c.ptr;
		}
		break;

	case C_SPACE:
		MAWK->rs_shadow.type = SEP_CHAR;
		MAWK->rs_shadow.c = ' ';
		break;

	case C_SNULL:								/* RS becomes one or more blank lines */
		if (MAWK->mawk_state == EXECUTION)
			MAWK->scan_code['\n'] = SC_SPACE;
		MAWK->rs_shadow.type = SEP_MLR;
		sval = mawk_new_STRING(MAWK, "\n\n+");
		MAWK->rs_shadow.ptr = mawk_re_compile(MAWK, sval);
		free_STRING(sval);
		break;

	default:
		mawk_bozo(MAWK, "bad cell in set_rs_shadow");
	}
}


void mawk_set_field0(mawk_state_t *MAWK, char *s, unsigned len)
{
	mawk_cell_destroy(MAWK, &MAWK->field[0]);
	MAWK->nf = -1;

	if (len) {
		MAWK->field[0].type = C_MBSTRN;
		MAWK->field[0].ptr = (PTR) mawk_new_STRING0(MAWK, len);
		memcpy(string(&MAWK->field[0])->str, s, len);
	}
	else {
		MAWK->field[0].type = C_STRING;
		MAWK->field[0].ptr = (PTR) & MAWK->null_str;
		MAWK->null_str.ref_cnt++;
	}
}



/* split field[0] into $1, $2 ... and set NF  */

void mawk_split_field0(mawk_state_t * MAWK)
{
	register mawk_cell_t *cp;
	register int cnt;
	mawk_cell_t c;												/* copy field[0] here if not string */


	if (MAWK->field[0].type < C_STRING) {
		mawk_cellcpy(MAWK, &c, MAWK->field + 0);
		mawk_cast1_to_str(MAWK, &c);
		cp = &c;
	}
	else
		cp = &MAWK->field[0];

	if (string(cp)->len == 0)
		MAWK->nf = 0;
	else {
		switch (MAWK->fs_shadow.type) {
		case C_SNULL:							/* FS == "" */
			MAWK->nf = mawk_null_split(MAWK, string(cp)->str);
			break;

		case C_SPACE:
			MAWK->nf = mawk_space_split(MAWK, string(cp)->str, string(cp)->len);
			break;

		default:
			MAWK->nf = mawk_re_split(MAWK, string(cp)->str, MAWK->fs_shadow.ptr);
			break;
		}

	}

	mawk_cell_destroy(MAWK, MAWK_NF);
	MAWK_NF->type = C_NUM;
	MAWK_NF->d.dval = (mawk_num_t) MAWK->nf;

	if (MAWK->nf > MAX_SPLIT) {
		cnt = MAX_SPLIT;
		load_field_ov(MAWK);
	}
	else
		cnt = MAWK->nf;

	while (cnt > 0) {
		mawk_cell_destroy(MAWK, MAWK->field + cnt);
		MAWK->field[cnt].ptr = (PTR) split_buff[cnt - 1];
		MAWK->field[cnt--].type = C_MBSTRN;
	}

	if (cp == &c) {
		free_STRING(string(cp));
	}
}

/* construct field[0] from the other fields */

static void build_field0(mawk_state_t * MAWK)
{
#ifdef DEBUG
	if (MAWK->nf < 0)
		mawk_bozo(MAWK, "nf <0 in build_field0");
#endif

	mawk_cell_destroy(MAWK, MAWK->field + 0);

	if (MAWK->nf == 0) {
		MAWK->field[0].type = C_STRING;
		MAWK->field[0].ptr = (PTR) & MAWK->null_str;
		MAWK->null_str.ref_cnt++;
	}
	else if (MAWK->nf == 1) {
		mawk_cellcpy(MAWK, MAWK->field, MAWK->field + 1);
	}
	else {
		mawk_cell_t c;
		mawk_string_t *ofs, *tail;
		unsigned len;
		register mawk_cell_t *cp;
		register char *p, *q;
		int cnt;
		mawk_cell_t **fbp, *cp_limit;


		mawk_cellcpy(MAWK, &c, OFS);
		mawk_cast1_to_str(MAWK, &c);
		ofs = (mawk_string_t *) c.ptr;
		mawk_cellcpy(MAWK, &c, field_ptr(MAWK->nf));
		mawk_cast1_to_str(MAWK, &c);
		tail = (mawk_string_t *) c.ptr;
		cnt = MAWK->nf - 1;

		len = cnt * ofs->len + tail->len;

		fbp = MAWK->fbank;
		cp_limit = MAWK->field + FBANK_SZ;
		cp = MAWK->field + 1;

		while (cnt-- > 0) {
			if (cp->type < C_STRING) {	/* use the string field temporarily */
				if (cp->type == C_NOINIT) {
					cp->ptr = (PTR) & MAWK->null_str;
					MAWK->null_str.ref_cnt++;
				}
				else {									/* its a number */

					Int ival;
					char xbuff[260];

					ival = mawk_d_to_I(cp->d.dval);
					if (ival == cp->d.dval)
						sprintf(xbuff, INT_FMT, ival);
					else
						sprintf(xbuff, string(MAWK_CONVFMT)->str, cp->d.dval);

					cp->ptr = (PTR) mawk_new_STRING(MAWK, xbuff);
				}
			}

			len += string(cp)->len;

			if (++cp == cp_limit) {
				cp = *++fbp;
				cp_limit = cp + FBANK_SZ;
			}

		}

		MAWK->field[0].type = C_STRING;
		MAWK->field[0].ptr = (PTR) mawk_new_STRING0(MAWK, len);

		p = string(MAWK->field)->str;

		/* walk it again , putting things together */
		cnt = MAWK->nf - 1;
		fbp = MAWK->fbank;
		cp = MAWK->field + 1;
		cp_limit = MAWK->field + FBANK_SZ;
		while (cnt-- > 0) {
			memcpy(p, string(cp)->str, string(cp)->len);
			p += string(cp)->len;
			/* if not really string, free temp use of ptr */
			if (cp->type < C_STRING) {
				free_STRING(string(cp));
			}
			if (++cp == cp_limit) {
				cp = *++fbp;
				cp_limit = cp + FBANK_SZ;
			}
			/* add the separator */
			q = ofs->str;
			while (*q)
				*p++ = *q++;
		}
		/* tack tail on the end */
		memcpy(p, tail->str, tail->len);

		/* cleanup */
		free_STRING(tail);
		free_STRING(ofs);
	}
}

/* We are assigning to a mawk_cell_t and we aren't sure if its
   a field */

void mawk_bifunct_target_assign(mawk_state_t *MAWK, register mawk_cell_t *target_, mawk_cell_t *source)
{
	mawk_cell_t *target = target_->ptr; /* target is assumed to be the varref */

	if (target_->type == C_ARR_REF_BT) {
		/* reference to an array member: ->ptr is the array, ->d.idx_str is the zmalloc'd string cell */
		mawk_array_set(MAWK, (mawk_array_t)target_->ptr, target_->d.idx_cell, source);
		mawk_cell_destroy(MAWK, target_->d.idx_cell);
		mawk_zfree(MAWK, target_->d.idx_cell, sizeof(mawk_cell_t));
		target_->type = C_NOINIT; /* don't need to destroy this: it's a special arr ref */
		return ;
	}

	/* original code dealing with a normal varref */
	if (target >= MAWK->field && target <= LAST_PFIELD)
		mawk_field_assign(MAWK, target, source);
	else {
		mawk_cell_t **p = MAWK->fbank + 1;

		while (*p) {
			if (target >= *p && target < *p + FBANK_SZ) {
				mawk_field_assign(MAWK, target, source);
				return;
			}
			p++;
		}
		/* its not a field */
		mawk_cell_destroy(MAWK, target);
		mawk_cellcpy(MAWK, target, source);
	}
}

/*
  $0 split into more than MAX_SPLIT fields,
  $(MAX_FIELD+1) ... are on the split_ov_list.
  Copy into fields which start at fbank[1]
*/

static void load_field_ov(mawk_state_t * MAWK)
{
	register SPLIT_OV *p;					/* walks split_ov_list */
	register mawk_cell_t *cp;						/* target of copy */
	int j;												/* current fbank[] */
	mawk_cell_t *cp_limit;								/* change fbank[] */
	SPLIT_OV *q;									/* trails p */

	/* make sure the fields are allocated */
	mawk_slow_field_ptr(MAWK, MAWK->nf);

	p = MAWK->split_ov_list;
	MAWK->split_ov_list = (SPLIT_OV *) 0;
	j = 1;
	cp = MAWK->fbank[j];
	cp_limit = cp + FBANK_SZ;
	while (p) {
		mawk_cell_destroy(MAWK, cp);
		cp->type = C_MBSTRN;
		cp->ptr = (PTR) p->sval;

		if (++cp == cp_limit) {
			cp = MAWK->fbank[++j];
			cp_limit = cp + FBANK_SZ;
		}

		q = p;
		p = p->link;
		MAWK_ZFREE(MAWK, q);
	}
}

/*
  assign mawk_cell_t *cp to field or pseudo field
  and take care of all side effects
*/
void mawk_field_assign(mawk_state_t *MAWK, register mawk_cell_t *fp, mawk_cell_t *cp)
{
	mawk_cell_t c;
	int i, j;

	/* the most common case first */
	if (fp == MAWK->field) {
		mawk_cell_destroy(MAWK, MAWK->field);
		mawk_cellcpy(MAWK, fp, cp);
		MAWK->nf = -1;
		return;
	}

	/* its not important to do any of this fast */

	if (MAWK->nf < 0)
		mawk_split_field0(MAWK);

	switch (i = (fp - MAWK->field)) {

	case MAWK_NF_field:

		mawk_cell_destroy(MAWK, MAWK_NF);
		mawk_cellcpy(MAWK, &c, cp);
		mawk_cellcpy(MAWK, MAWK_NF, &c);
		if (c.type != C_NUM)
			mawk_cast1_to_num(MAWK, &c);

		if ((j = d_to_i(c.d.dval)) < 0)
			mawk_rt_error(MAWK, "negative value assigned to NF");

		if (j > MAWK->nf)
			for (i = MAWK->nf + 1; i <= j; i++) {
				cp = field_ptr(i);
				mawk_cell_destroy(MAWK, cp);
				cp->type = C_STRING;
				cp->ptr = (PTR) & MAWK->null_str;
				MAWK->null_str.ref_cnt++;
			}

		MAWK->nf = j;
		build_field0(MAWK);
		break;

	case MAWK_RS_field:
		mawk_cell_destroy(MAWK, MAWK_RS);
		mawk_cellcpy(MAWK, MAWK_RS, cp);
		set_rs_shadow(MAWK);
		break;

	case MAWK_FS_field:
		mawk_cell_destroy(MAWK, MAWK_FS);
		mawk_cellcpy(MAWK, MAWK_FS, cp);
		mawk_cellcpy(MAWK, &MAWK->fs_shadow, MAWK_FS);
		mawk_cast_for_split(MAWK, &MAWK->fs_shadow);
		break;

	case MAWK_OFMT_field:
	case MAWK_CONVFMT_field:
		/* If the user does something stupid with OFMT or CONVFMT,
		   we could crash.
		   We'll make an attempt to protect ourselves here.  This is
		   why OFMT and CONVFMT are pseudo fields.

		   The ptrs of OFMT and CONVFMT always have a valid mawk_string_t,
		   even if assigned a NUM or NOINIT
		 */

		free_STRING(string(fp));
		mawk_cellcpy(MAWK, fp, cp);
		if (fp->type < C_STRING)		/* !! */
			fp->ptr = (PTR) mawk_new_STRING(MAWK, "%.6g");
		else if (fp == MAWK_CONVFMT) {
			/* It's a string, but if it's really goofy and CONVFMT,
			   it could still mawk_damage us. Test it .
			 */
			char xbuff[512];

			xbuff[256] = 0;
			sprintf(xbuff, string(fp)->str, 3.1459);
			if (xbuff[256])
				mawk_rt_error(MAWK, "CONVFMT assigned unusable value");
		}
		break;

	default:											/* $1 or $2 or ... */
		mawk_cell_destroy(MAWK, fp);
		mawk_cellcpy(MAWK, fp, cp);

		if (i < 0 || i > MAX_SPLIT)
			i = mawk_field_addr_to_index(MAWK, fp);

		if (i > MAWK->nf) {
			for (j = MAWK->nf + 1; j < i; j++) {
				cp = field_ptr(j);
				mawk_cell_destroy(MAWK, cp);
				cp->type = C_STRING;
				cp->ptr = (PTR) & MAWK->null_str;
				MAWK->null_str.ref_cnt++;
			}
			MAWK->nf = i;
			mawk_cell_destroy(MAWK, MAWK_NF);
			MAWK_NF->type = C_NUM;
			MAWK_NF->d.dval = (mawk_num_t) i;
		}

		build_field0(MAWK);

	}
}
