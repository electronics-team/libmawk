/********************************************
da_common.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

/* disassemble code: common for text and bin */

#include <string.h>
#include  "mawk.h"


#include  "code.h"
#include  "bi_funct.h"
#include  "repl.h"
#include  "field.h"
#include  "num.h"

static const struct {
	PF_CP action;
	char *name;
} special_cases[] =	{ /* read-only */
	{mawk_bi_split,   "split"},
	{mawk_bi_match,   "match"},
	{mawk_bi_getline, "getline"},
	{mawk_bi_sub,     "sub"},
	{mawk_bi_gsub,    "gsub"},
	{mawk_bi_print,   "print"},
	{mawk_bi_printf,  "printf"},
	{(PF_CP) 0, NULL}
};


const char *mawk_find_bi_name(PF_CP p)
{
	const BI_REC *q;
	int i;

	for (q = mawk_bi_funct; q->name; q++) {
		if (q->fp == p) {
			/* found */
			return q->name;
		}
	}
	/* next check some special cases */
	for (i = 0; special_cases[i].action; i++) {
		if (special_cases[i].action == p)
			return special_cases[i].name;
	}

	return "unknown builtin";
}

PF_CP mawk_find_bi_ptr(const char *name)
{
	const BI_REC *q;
	int i;

	for (q = mawk_bi_funct; q->name; q++) {
		if (strcmp(q->name, name) == 0) {
			/* found */
			return q->fp;
		}
	}
	/* next check some special cases */
	for (i = 0; special_cases[i].action; i++) {
		if (strcmp(special_cases[i].name, name) == 0)
			return special_cases[i].action;
	}

	return NULL;
}

void mawk_add_to_fdump_list(mawk_state_t *MAWK, FBLOCK *fbp)
{
	struct mawk_fdump *p = MAWK_ZMALLOC(MAWK, struct mawk_fdump);
	p->fbp = fbp;
	p->link = MAWK->fdump_list;
	MAWK->fdump_list = p;
}
