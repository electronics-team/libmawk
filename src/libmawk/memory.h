
/********************************************
memory.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  MEMORY_H
#define  MEMORY_H

#include <libmawk/zmalloc.h>


mawk_string_t *mawk_new_STRING(mawk_state_t *, const char *);
mawk_string_t *mawk_new_STRING0(mawk_state_t *, unsigned);

#ifdef   DEBUG
void DB_free_STRING(mawk_state_t *, mawk_string_t *);

#define  free_STRING(s)  DB_free_STRING(MAWK, s)

#else

#define  free_STRING(sval)   if ( -- (sval)->ref_cnt == 0 )\
                                mawk_zfree(MAWK, sval, (sval)->len+STRING_OH) ; else
#endif

#ifdef	 DEBUG
void DB_mawk_eval_overflow(mawk_state_t * MAWK);

#define	 inc_sp()       if( ++sp == MAWK->eval_stack+EVAL_STACK_SIZE )\
			 DB_mawk_eval_overflow(MAWK)
#define	 inc_mawksp()   if( ++(MAWK->sp) == MAWK->eval_stack+EVAL_STACK_SIZE )\
			 DB_mawk_eval_overflow(MAWK)
#else

/* If things are working, the eval stack should not mawk_overflow */

#define inc_sp()        sp++
#define inc_mawksp()    (MAWK->sp)++
#endif


/* large block allocation */
void *mawk_malloc(mawk_state_t *MAWK, int size);
void *mawk_realloc(mawk_state_t *MAWK, void *ptr, int size);
void mawk_free(mawk_state_t *MAWK, void *ptr);
void mawk_free_all(mawk_state_t *MAWK);
char *mawk_strdup(mawk_state_t *MAWK, const char *s);

/* plain malloc() wrappers */
char *mawk_strdup_(const char *s);


#endif /* MEMORY_H */
