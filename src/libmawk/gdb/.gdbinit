
define abreak
	break mawk_breakpoint if MAWK->token_lineno == $arg0
end

define astep
	set lineno = MAWK->token_lineno
	break mawk_breakpoint if MAWK->token_lineno == $lineno
end

define awhere
	call mawk_debug_where(MAWK)
end

echo Debugging libmawk awk code\n

abreak 2
run
