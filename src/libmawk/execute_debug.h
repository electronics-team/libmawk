/* this is not a real header and is included in the middle of execute.c to
   provide real or dummy call debug functionality */
/*#define CALLDEBUG*/

#ifdef CALLDEBUG
/* real prints */
#include <stdio.h>
#define db1printf(x...) fprintf(stderr, x)
#define stackptr(p) ((p) == NULL ? -1 : (p)-MAWK->eval_stack)
extern void mawk_print_cell(mawk_state_t *, mawk_cell_t *, FILE_NODE *);
static void db1printstack(mawk_state_t *MAWK, char *ann, mawk_cell_t *sp, mawk_cell_t *fp)
{
	mawk_cell_t *c;
	db1printf("%s\n", ann);
	for(c = sp; c >= MAWK->eval_stack; c--) {
		if (fp == c)
			db1printf("*");
		else
			db1printf(" ");
		db1printf("[%d] ", stackptr(c));
		if (c->type == C_EXE_STTYPE) {
			db1printf("exest=");
			switch(c->d.vcnt) {
				case EXEST_NORMAL: db1printf("EXEST_NORMAL\n"); break;
				case EXEST_EXIT:   db1printf("EXEST_EXIT\n"); break;
				case EXEST_RANGE1: db1printf("EXEST_RANGE1\n"); break;
				case EXEST_RANGE2: db1printf("EXEST_RANGE2\n"); break;
				default:           db1printf("EXEST_%d\n", c->d.vcnt); break;
			}
		}
		else if (c->type == C_EXE_STATE)
			db1printf("state=%p\n", c->ptr);
		else if (c->type == C_ARR_REF)
			db1printf("date=arr_ref=%p\n", c->ptr);
		else {
			db1printf("data=%d=", c->type);
			mawk_print_cell(MAWK, c, MAWK->fnode_stderr);
			db1printf("\n");
		}
	}
}
#else

/* dummy prints */
static void db1printf(const char *fmt, ...) { }
#define stackptr(p) 0
#define db1printstack(mawk, ann, sp, fp)
#endif

