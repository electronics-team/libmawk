/********************************************
libmawk (C) 2009-2014, Tibor 'Igor2' Palinkas;

This is a source file for libmawk, an implementation of
the AWK programming language, fork of mawk.

Libmawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include <stdio.h>
#include "mawk.h"

/* static mawk_state_t *mawk_debugging = NULL; */

void mawk_breakpoint(mawk_state_t * MAWK)
{

}

void mawk_location_change(mawk_state_t * MAWK, int new_token_line)
{
	MAWK->token_lineno = new_token_line;
/*	mawk_debugging = MAWK; */
	mawk_breakpoint(MAWK);
}

void mawk_debug_callstack_push(mawk_state_t * MAWK, FBLOCK * f)
{
	mawk_debug_callstack_t *s;

/*	printf("PUSH: %s\n", f->name); */
	s = malloc(sizeof(mawk_debug_callstack_t));
	s->f = f;
	s->next = MAWK->debug_callstack;
	MAWK->debug_callstack = s;
}

void mawk_debug_callstack_pop(mawk_state_t * MAWK)
{
	mawk_debug_callstack_t *s = MAWK->debug_callstack;

	if (s != NULL) {
		MAWK->debug_callstack = MAWK->debug_callstack->next;
		free(s);
	}
}

void mawk_debug_where(mawk_state_t * MAWK)
{
	mawk_debug_callstack_t *s;
	int n;

	printf("mawk call stack:\n");
	for (n = 0, s = MAWK->debug_callstack; s != NULL; s = s->next, n++) {
		printf(" #%d %s\n", n, s->f->name);
	}
}

/*                                name     code  size nargs  typev */
const FBLOCK mawk_debug_main  = { "MAIN",  NULL, 0,   0,     "" };
const FBLOCK mawk_debug_begin = { "BEGIN", NULL, 0,   0,     "" };
