
/********************************************
bi_funct.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  BI_FUNCT_H
#define  BI_FUNCT_H  1

#include <libmawk/symtype.h>

extern const BI_REC mawk_bi_funct[];	/* read-only */

/* builtin string functions */
mawk_cell_t *mawk_bi_print(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_printf(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_length(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_index(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_substr(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_sprintf(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_split(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_match(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_getline(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_sub(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_gsub(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_toupper(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_tolower(mawk_state_t *, mawk_cell_t *);

/* builtin arith functions */
mawk_cell_t *mawk_bi_sin(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_cos(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_atan2(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_log(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_exp(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_int(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_sqrt(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_srand(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_rand(mawk_state_t *, mawk_cell_t *);

/* other builtins */
mawk_cell_t *mawk_bi_close(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_system(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_fflush(mawk_state_t *, mawk_cell_t *);

/* libmawk extensions */
mawk_cell_t *mawk_bi_call(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_acall(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_valueof(mawk_state_t *, mawk_cell_t *);
mawk_cell_t *mawk_bi_isnan(mawk_state_t *, mawk_cell_t *);

#endif /* BI_FUNCT_H  */
