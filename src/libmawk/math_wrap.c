/********************************************
libmawk (C) 2009-2014, Tibor 'Igor2' Palinkas;

This is a source file for libmawk, an implementation of
the AWK programming language, fork of mawk.

Libmawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include <math.h>
#include <errno.h>
#include "math_wrap.h"

int PM_errno;

double P_log_(double x, int *perrno)
{
	double ret;
#ifdef P_MBROKEN_LOG_M_0
	if (x == -0.0) {
		*perrno = -1;
		return 0;
	}
#endif

#ifdef P_MBROKEN_LOG_P_0
	if (x == +0.0) {
		*perrno = -1;
		return 0;
	}
#endif

#ifdef P_MBROKEN_LOG_P_1
	if (x == +1.0) {
		*perrno = -1;
		return 0;
	}
#endif

/* we don't have portable NaN, scconfig doesn't yet detect FP_NAN, we
   have to emulate one by hand */
	if (x < 0) {
		*perrno = -123;
		return 0;
	}
	ret = log(x);
	if (errno != 0)
		*perrno = errno;
	return ret;
}


double P_divf_(double x, double y, int *perrno)
{
	if (y == 0) {
		*perrno = -123;
		return 0;
	}
	return x/y;
}
