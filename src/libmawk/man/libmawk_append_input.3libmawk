.\" Copyright 2009 Tibor Palinkas (mawk@inno.bme.hu)
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\"
.TH LIBMAWK_APPEND_INPUT 3libmawk  2009-08-10 "libmawk" "libmawk manual"
.SH NAME
libmawk_append_input \- append a string to an input buffer
.SH SYNOPSIS
.nf
.B #include <libmawk.h>
.sp
.BI "void libmawk_append_input(mawk_state_t *" m ", const char *" input_str );
.fi
.sp
.BI "void libmawk_append_ninput(mawk_state_t *" m ", const char *" input ", int" len );
.fi
.sp
.SH DESCRIPTION
The
.BR libmawk_append_input ()
and
.BR libmawk_append_ninput ()
functions allow the application to fill the input buffer of a libmawk context.
No record separator is appended, only the bytes donated by input_str or input,
thus it is possible to append partial records. Appending to
the input doesn't have the side effect of any script being run. There may be
multiple libmawk_append_input() calls before a call to libmawk_run_main(). The
latter all is used to let the script process the input buffer.
.sp
The only difference between the two calls are the input format:
.BR libmawk_append_input ()
expects a nul-terminated string, whereas
.BR libmawk_append_ninput ()
takes an arbitrary binary data and its length.
.sp
Argument m is a libmawk context previously returned by libmawk_initialize()
or libmawk_initialize_stage3().
.SH "SEE ALSO"
.BR libmawk_initialize_stage (3libmawk),
.BR libmawk_initialize (3libmawk),
.BR libmawk_run_main (3libmawk).

