1. Introduction

Libmawk is a fork of mawk 1.3.3, restructured for embedding. This means the
user gets libmawk.h and libmawk.so and can embed awk scripting language
in any application written in C or in any language that can use libraries
written in C. For more information, check out the web page at
http://repo.hu/projects/libmawk and the documentation in doc/.

2. Requirements and compiling

ANSI C compiler, POSIX shell and make are required for compiling libmawk.
Bison should be installed for developing libmawk; if it is not installed,
local changes to the grammar will be ingored.

./configure; make

On top of usual scconfig arguments, ./configure accepts --numeric=TYPE,
where TYPE is int or double. Default is double. This switch affects
what type libmawk stores numbers in.

3. installation

Run "make install" or "make linstall". The linstall version
will use symlinks instead of actual copying of files which is useful if
you develop libmawk, the library itself.

4. Compatibility with mawk

Compatibility with mawk is maintained to some degree. Currently libmawk
offers a small set of extra features on awk level while providing
everything that mawk provides. A valid mawk script will work with
libmawk/lmawk without modification. However, the new features will work
only with libmawk/lmawk and not mawk, so portable scripts shouldn't
depend on them. All extensions are clearly marked in the manual.

Conclusion: libmawk will compile and install mawk executable, which is
backward compatible with mawk executable but also adds some extension
features.

Awklib depends on one of the libmawk features (include), thus awklib
scripts won't work with other awk implementations without tweaking.
