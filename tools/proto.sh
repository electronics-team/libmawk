#!/bin/sh
for n in *.h *.c
do
	sed "/PROTO(/ { s/PROTO(//; s/[,][ 	]*[(]/(/; s/);[ 	]*$/;/; }" < $n > $n.tmp
	mv $n.tmp $n
done
