#!/bin/sh

# read the outpot of chlog.sh and split it per topic and save the lines
# into changelog-formatted files

awk '
	BEGIN {
		IGNORE["todo"]=1
		IGNORE["devlog"]=1
		IGNORE["bugreport"]=1
		IGNORE["blog_queue"]=1
	}
	
	($1 ~ "^[[][^]]*\]$") {
		tag=tolower($1)
		sub("[[]", "", tag)
		sub("\]", "", tag)

		if (tag in IGNORE)
			next

		$1=""
		line=$0
		if (!(tag in SEEN)) {
			SEEN[tag]++
		}
		print "	[" tag "]" line > "CHG." tag
		next
	}
	{
		line=$0
		print line > "CHG.MISC"
	}
'
