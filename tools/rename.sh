#!/bin/sh

for n in *.h *.c *.y
do
	case $n in 
		parse.c) ;;
		mawk.h);;
		*)
			mv $n $n.old
			sed "s/\([^A-Za-z_>]\)$1/\1mawk_$1/g;s/^$1/mawk_$1/" < $n.old > $n
			rm -f $n.old
			;;
	esac
done
